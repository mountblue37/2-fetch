
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
function fetchAllData(url) {
    return new Promise((resolve, reject) => {
        fetch(url).then((response) => {
                return response.json()
            })
            .then((data) => {
                resolve(data)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

function fetchUser(url, id) {
    return new Promise((resolve, reject) => {
        fetch(url + id).then((response) => {
                return response.json()
            })
            .then((data) => {
                resolve(data)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

// 1. Fetch all the users
function problem1() {
    const usersURL = 'https://jsonplaceholder.typicode.com/users'
    fetchAllData(usersURL).then((usersData) => {
            console.log(usersData)
        })
        .catch((err) => {
            console.error(err)
        })
}

// problem1()

// 2. Fetch all the todos
function problem2() {
    const todosURL = 'https://jsonplaceholder.typicode.com/todos'
    fetchAllData(todosURL).then((todosData) => {
            console.log(todosData)
        })
        .catch((err) => {
            console.error(err)
        })
}

// problem2()

// 3. Use the promise chain and fetch the users first and then the todos.
function problem3() {
    const usersURL = 'https://jsonplaceholder.typicode.com/users'
    const todosURL = 'https://jsonplaceholder.typicode.com/todos'
    fetchAllData(usersURL).then((usersData) => {
            console.log(usersData)
        })
        .then(() => {
            return fetchAllData(todosURL)
        })
        .then((todosData) => {
            console.log(todosData)
        })
        .catch((err) => {
            console.error(err)
        })
}

// problem3()

// 4. Use the promise chain and fetch the users first and then all the details for each user.
function problem4() {
    const usersURL = 'https://jsonplaceholder.typicode.com/users'
    const singleUser = 'https://jsonplaceholder.typicode.com/users?id='
    fetchAllData(usersURL).then((usersData) => {
            return usersData
        })
        .then((usersData) => {
            console.log('usersData', usersData)
            return Promise.all(usersData.map((user) => {
                return fetchUser(singleUser, user.id)
            }))
        })
        .then((userDetails) => {
            console.log('singleUser', userDetails)
        })
        .catch((err) => {
            console.error(err)
        })
}

// problem4()  

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo
function problem5() {
    const todosURL = 'https://jsonplaceholder.typicode.com/todos'
    const singleUser = 'https://jsonplaceholder.typicode.com/users?id='
    fetchAllData(todosURL).then((todosData) => {
            return todosData[0]
        })
        .then((todo) => {
            console.log(todo)
            return fetchUser(singleUser, todo.userId)
        })
        .then((user) => {
            console.log(user)
        })
        .catch((err) => {
            console.error(err)
        })
}

problem5()
